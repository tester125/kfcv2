<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Autocomplete extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
	}
	public function search()
	{
		// tangkap variabel keyword dari URL
		$keyword = $this->uri->segment(3);

		// cari di database
		$data = $this->db->from('tb_jemaat')->like('nama_jemaat',$keyword)->get();	

		// format keluaran di dalam array
		foreach($data->result() as $row)
		{
			$arr['query'] = $keyword;
			$arr['suggestions'][] = array(
				'value'	=>$row->nama_jemaat,
				'nama_jemaat'	=>$row->nama_jemaat,
				'alamat_jemaat'	=>$row->alamat_jemaat,
				'tlp_jemaat'	=>$row->tlp_jemaat,
				'id_kfc'	=>$row->id_kfc
			//// alias                  nama field pada tabel
			);
		}
		// minimal PHP 5.2
		echo json_encode($arr);
	}
}
?>