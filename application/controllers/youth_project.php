<?php
class youth_project extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->model('m_youth_database');
	}
	function index(){
		$session=$this->session->userdata('sudahLogin');
		if($session==false){
			$this->load->view('signin');
		}else{
			redirect('dashboard02');
		}
	}
	function autentifikasi(){
		$username=$this->input->post("username");
		$password=$this->input->post("password");
		$cek=$this->m_youth_database->cek_admin($username,md5($password));
		if(count($cek)==1){
			$this->session->set_userdata(array(
			'sudahLogin'=>true,
			'username'=>$username));
			redirect('dashboard02','refresh');
		}else{
			echo"<script>alert('username atau password anda salah silahkan coba lagi')</script>";
			redirect('youth_project','refresh');
		}
	}
}
?>