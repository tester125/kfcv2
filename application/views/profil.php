<div class="main-panel">
            <nav class="navbar navbar-transparent navbar-absolute">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#"> Profil </a>
                    </div>
                    <div class="collapse navbar-collapse">
                        <ul class="nav navbar-nav navbar-right">
                            <li>
                                <a href="#pablo" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="material-icons">dashboard</i>
                                    <p class="hidden-lg hidden-md">Dashboard</p>
                                </a>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="material-icons">notifications</i>
                                    <span class="notification">5</span>
                                    <p class="hidden-lg hidden-md">Notifications</p>
                                </a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <a href="#">Mike John responded to your email</a>
                                    </li>
                                    <li>
                                        <a href="#">You have 5 new tasks</a>
                                    </li>
                                    <li>
                                        <a href="#">You're now friend with Andrew</a>
                                    </li>
                                    <li>
                                        <a href="#">Another Notification</a>
                                    </li>
                                    <li>
                                        <a href="#">Another One</a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="#pablo" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="material-icons">person</i>
                                    <p class="hidden-lg hidden-md">Profile</p>
                                </a>
                            </li>
                        </ul>
                        <form class="navbar-form navbar-right" role="search">
                            <div class="form-group  is-empty">
                                <input type="text" class="form-control" placeholder="Search">
                                <span class="material-input"></span>
                            </div>
                            <button type="submit" class="btn btn-white btn-round btn-just-icon">
                                <i class="material-icons">search</i>
                                <div class="ripple-container"></div>
                            </button>
                        </form>
                    </div>
                </div>
            </nav>
            <div class="content">
                <div class="container-fluid">
					
                    <div class="row">
                        
						<div class="col-md-4">
                            <!--mulai edit-->
                            <div class="card card-profile">
                               

                                <div class="card-avatar " data-background-color="blue">
                                    <!--h4 class="title"><?php foreach($data_profil as $data){ echo $data->nama_jemaat;}?>Data Presensi</h4-->
                                    <img class="img" src="<?php 
                                    echo base_url();?>foto/<?php foreach($data_profil as $data){ 
                                        
                                        if($data->foto==null){
                                            echo "user.jpg";
                                        }else{echo $data->foto;}
                                    }
                                        ?>" width="100px">
                                    <div class="row">
                                        <!--div class="col-md-4">
                                            <p>fotonya <?php //foreach($data_profil as $data){ echo $data->nama_jemaat;}?></p><br>
                                            
                                            </div>
                                        <div class="col-md-7">
                                            <p>fotonya <?php //foreach($data_profil as $data){ echo $data->id_kfc;}?></p>
                                                    
                                        </div-->

                                    </div>
									

                                </div>

                                <div class="card-content card-body ">
                                    <h6 class="card-category text-gray">CEO / Co-Founder</h6>
                                    <h4 class="card-title"><?php foreach($data_profil as $data){ echo $data->nama_jemaat;}?></h4>
                                    <h6 class="card-category text-danger <?php foreach($data_profil as $data){ 
                                        if($data->nama_jemaat=="0"){echo "text-danger";}else{echo "text-info";}}?>"><?php foreach($data_profil as $data){ 
                                        if($data->nama_jemaat=="0"){echo "Not Active";}else{echo "Active";}}?></h6>
                                    <p class="card-description">
                    Don't be scared of the truth because we need to restart the human foundation in truth And I love you like Kanye loves Kanye I love Rick Owens’ bed design but the back is...
                  </p>
                  <a href="<?php foreach($data_profil as $data){ echo base_url('index.php/dashboard02/edit_profil/'); echo $data->id_kfc;} ?>" class="btn btn-just-icon btn-danger btn-round">Edit
                                                        <i class="fa fa-edit"></i>
                                                    </a>


                                </div>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="card card-nav-tabs">
                                <div class="card-header" data-background-color="green">
                                    <h4><?php foreach($data_profil as $data){ echo $data->id_kfc;}?></h4>
                                    <!--img src="<?php echo base_url();?>foto/komsel/<?php foreach($data_profil as $data){ 
                                        if($data->komsel=='unique'){
                                            echo 'unique.png';
                                    }else{if($data->komsel=='banyak'){
                                            echo 'banyak.png';
                                    }else{if($data->komsel=='jco'){
                                            echo 'jco.png';
                                    }else{if($data->komsel=='rock'){
                                            echo 'rock.png';
                                    }else{if($data->komsel=='gamalove'){
                                            echo 'gamalove.png';
                                    }else{echo 'pemenang.png';}}}
                                }
                                    }
                                        }?>" -->
                                    
                                </div>
                                <div class="card-content">
                                    <div class="tab-content">
                                        <div class="tab-pane active">
                                            <div class="row">
                                                <div class="col-md-4 ">
                                                    <p>Gender</p><br>
                                                    <p>Tangal Lahir</p><br>
                                                    <p>Alamat</p><br>
                                                    <p>Tlp</p><br>
                                                    <p>Wa</p><br>
                                                    <p>Komsel</p><br>
                                                    <p>Facebook/ Instagram</p><br>
                                                    <p>Reg Date</p><br>
                                                    <p>Pemurid</p><br>
                                                    <p>M1</p><br>
                                                    <p>Pelayanan</p><br>
                                                </div>
                                                <div class="col-md-8">
                                                    <p><?php foreach($data_profil as $data){ echo $data->jenis_kelamin;}?></p><br>
                                                    <p><?php foreach($data_profil as $data){if($data->tgl_masuk==null){echo "00/00/0000";}else{echo $data->tgl_lahir;} }?></p><br>
                                                    <p><?php foreach($data_profil as $data){ if($data->tgl_masuk==null){echo "-";}else{echo $data->alamat_jemaat;}}?></p><br>
                                                    <p><?php foreach($data_profil as $data){ if($data->tgl_masuk==null){echo "-";}else{echo $data->tlp_jemaat;}}?></p><br>
                                                    <p><?php foreach($data_profil as $data){ if($data->tgl_masuk==null){echo "-";}else{echo $data->bbm_wa;}}?></p><br>
                                                    <p><?php foreach($data_profil as $data){ if($data->tgl_masuk==null){echo "-";}else{echo $data->komsel;}}?></p><br>
                                                    <p><?php foreach($data_profil as $data){ if($data->tgl_masuk==null){echo "-";}else{echo $data->fb_ig;}}?></p><br>
                                                    <p><?php foreach($data_profil as $data){ 
                                                        if($data->tgl_masuk==null){echo "00/00/0000";}else{echo $data->tgl_masuk;} }?></p><br>
                                                    <p><?php foreach($data_profil as $data){if($data->tgl_masuk==null){echo "-";}else{echo $data->pemurid;}}?></p><br>
                                                    <p><?php foreach($data_profil as $data){if($data->tgl_masuk==null){echo "-";}else{echo $data->m1;}}?></p><br>
                                                    <p><?php foreach($data_profil as $data){if($data->tgl_masuk==null){echo "-";}else{echo $data->alokasi;}}?></p><br>
                                                </div>
                                            </div>
                                            <!--table class="table">
                                                <tr>
                                                    <td>Gender</td><td><?php foreach($data_profil as $data){ echo $data->jenis_kelamin;}?></td>
                                                </tr>
                                                <tr>
                                                    <td>Tangal Lahir</td><td><?php foreach($data_profil as $data){ echo $data->tgl_lahir;}?></td>
                                                </tr>
                                                <tr>
                                                    <td>Alamat</td><td><?php foreach($data_profil as $data){ echo $data->alamat_jemaat;}?></td>
                                                </tr>
                                                <tr>
                                                    <td>No Telepon</td><td><?php foreach($data_profil as $data){ echo $data->tlp_jemaat;}?></td>
                                                </tr>
                                                <tr>
                                                    <td>WhatsApp</td><td><?php foreach($data_profil as $data){ echo $data->bbm_wa;}?></td>
                                                </tr>
                                                <tr>
                                                    <td>Komsel</td><td><?php foreach($data_profil as $data){ echo $data->komsel;}?></td>
                                                </tr>
                                                <tr>
                                                    <td>Facebook/Instagram</td><td><?php foreach($data_profil as $data){ echo $data->fb_ig;}?></td>
                                                </tr>
                                                <tr>
                                                    <td style="text-style:underline;">Last Presensi</td><td><?php foreach($data_profil as $data){ echo $data->last_presensi;}?></td>
                                                </tr>
                                                <tr>
                                                    <td>Register Date</td><td><?php foreach($data_profil as $data){ echo $data->tgl_masuk;}?></td>
                                                </tr>
                                                <tr>
                                                    <td>Follow Up</td><td>None</td>
                                                </tr>
                                            </table-->
                                            
                                            <div class="card-footer justify-content-center card-profile">
                                                    <a href="https://api.whatsapp.com/send?phone=62<?php foreach($data_profil as $data){ echo ltrim(($data->tlp_jemaat),'0');}?>&text=Happy Birthday" class="btn btn-just-icon btn-whatsapp btn-round" data-background-color="green">
                                                        <i class="fa fa-whatsapp"></i>
                                                    </a>
                                                    
                                                    <a href="tel:<?php foreach($data_profil as $data){ echo $data->tlp_jemaat;}?>" class="btn btn-just-icon btn-google btn-round">
                                                        <i class="fa fa-address-book-o"></i>
                                                    </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
						
                    </div>
                </div>
            </div>
