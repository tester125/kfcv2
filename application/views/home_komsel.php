<!--body-->
<div class="container">
<div="jumbotron">
<img src="<?php echo base_url('/jumbotroncp.jpg');?>" align="center" width="100%" height="25%"></img>
</div>
</div>

<div class="container">
	<div class="well">	
		<fieldset>

		<!-- Form Name -->
		<legend>Info Komsel Youth GBI Gajah Mada...!</legend>
			<div class="row row-padded hompage-gridrow-bordered p-t text-center">
				<div class="col-md-6">
				<div class="panel panel-default">
                    <div class="panel-heading">
					<h5>JCo</h5>
					</div>
					<table class="table table-sm table-inverse">
					  <thead>
						<tr>
						  <th>No.</th>
						  <th>Nama</th>
						  <th>Gender</th>
						  <th>Alamat</th>
						</tr>
					  </thead>
					  <tbody>
						<?php
								$no=1;
								if($jco==NULL){?>
							<div class="alert alert-error">
								<button type="button" class="close" data-dismiss="alert">x</button>
								<h4>Peringatan</h4>
								<p>Peringatan</p>
							</div>
							<?php }else{foreach($jco as $db){?>
							<tr class="btn-success">
								<td><?php echo $no++;?></td>
								<td><?php echo $db->nama_jemaat;?></td>
								<td><?php echo $db->jenis_kelamin;?></td>
								<td><?php echo $db->tgl_masuk;?></td>
							</tr>
							<?php	}	}?>
					  </tbody>
					</table>
				</div>
				</div>
                <div class="col-md-6">
				<div class="panel panel-default">
                    <div class="panel-heading">
					<h5>UNIQUE</h5>
					</div>
					<table class="table table-sm table-inverse">
					  <thead>
						<tr>
						  <th>No.</th>
						  <th>Nama</th>
						  <th>Gender</th>
						  <th>Alamat</th>
						</tr>
					  </thead>
					  <tbody>
						<?php
								$no=1;
								if($unique==NULL){?>
							<div class="alert alert-error">
								<button type="button" class="close" data-dismiss="alert">x</button>
								<h4>Peringatan</h4>
								<p>Peringatan</p>
							</div>
							<?php }else{foreach($unique as $db){?>
							<tr class="btn-success">
								<td><?php echo $no++;?></td>
								<td><?php echo $db->nama_jemaat;?></td>
								<td><?php echo $db->jenis_kelamin;?></td>
								<td><?php echo $db->tgl_masuk;?></td>
							</tr>
							<?php	}	}?>
					  </tbody>
					</table>
				</div>
				</div>
				<!--ganti baris-->
				<div class="col-md-6">
				<div class="panel panel-default">
                    <div class="panel-heading">
					<h5>ROCK</h5>
					</div>
					<table class="table table-sm table-inverse">
					  <thead>
						<tr>
						  <th>No.</th>
						  <th>Nama</th>
						  <th>Gender</th>
						  <th>Alamat</th>
						</tr>
					  </thead>
					  <tbody>
						<?php
								$no=1;
								if($rock==NULL){?>
							<div class="alert alert-error">
								<button type="button" class="close" data-dismiss="alert">x</button>
								<h4>Peringatan</h4>
								<p>Peringatan</p>
							</div>
							<?php }else{foreach($rock as $db){?>
							<tr class="btn-success">
								<td><?php echo $no++;?></td>
								<td><?php echo $db->nama_jemaat;?></td>
								<td><?php echo $db->jenis_kelamin;?></td>
								<td><?php echo $db->tgl_masuk;?></td>
							</tr>
							<?php	}	}?>
					  </tbody>
					</table>
				</div>
				</div>
				
				<div class="col-md-6">
				<div class="panel panel-default">
                    <div class="panel-heading">
					<h5>Pemenang</h5>
					</div>
					<table class="table table-sm table-inverse">
					  <thead>
						<tr>
						  <th>No.</th>
						  <th>Nama</th>
						  <th>Gender</th>
						  <th>Alamat</th>
						</tr>
					  </thead>
					  <tbody>
						<?php
								$no=1;
								if($pemenang==NULL){?>
							<div class="alert alert-error">
								<button type="button" class="close" data-dismiss="alert">x</button>
								<h4>Peringatan</h4>
								<p>Peringatan</p>
							</div>
							<?php }else{foreach($pemenang as $db){?>
							<tr class="btn-success">
								<td><?php echo $no++;?></td>
								<td><?php echo $db->nama_jemaat;?></td>
								<td><?php echo $db->jenis_kelamin;?></td>
								<td><?php echo $db->tgl_masuk;?></td>
							</tr>
							<?php	}	}?>
					  </tbody>
					</table>
				</div>
				</div>
				<!--ganti baris-->
				<div class="col-md-6">
				<div class="panel panel-default">
                    <div class="panel-heading">
					<h5>Gama Love</h5>
					</div>
					<table class="table table-sm table-inverse">
					  <thead>
						<tr>
						  <th>No.</th>
						  <th>Nama</th>
						  <th>Gender</th>
						  <th>Alamat</th>
						</tr>
					  </thead>
					  <tbody>
						<?php
								$no=1;
								if($gama_love==NULL){?>
							<div class="alert alert-error">
								<button type="button" class="close" data-dismiss="alert">x</button>
								<h4>Peringatan</h4>
								<p>Peringatan</p>
							</div>
							<?php }else{foreach($gama_love as $db){?>
							<tr class="btn-success">
								<td><?php echo $no++;?></td>
								<td><?php echo $db->nama_jemaat;?></td>
								<td><?php echo $db->jenis_kelamin;?></td>
								<td><?php echo $db->tgl_masuk;?></td>
							</tr>
							<?php	}	}?>
					  </tbody>
					</table>
				</div>
				</div>
				<!--abis-->
			</div>

		</fieldset>

</div>
</div>
    </div><!-- /.container -->
<!--end body-->