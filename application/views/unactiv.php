<!--body-->
<div class="container ">
<!--isi web-->
	<!--div class="jumbotron"-->
		<div class="row">
			<div class="well">
			<legend style="color:red;">Jemaat Tidak Aktif...!</legend>
				<section>
					<table class="table table-condensed table-hover table-responsive">
						<thead>
							<tr>
								<th>No.</th>
								<th>Nama</th>
								<th>Gender</th>
								<th>Tanggal Lahir</th>
								<th>Alamat</th>
								<th>Telepon</th>
								<th>Bbm/WA</th>
								<th></th>
							</tr>
						</thead>
							<tbody>
							<?php
								$no=1;
								if($data_get==NULL){?>
							<div class="alert alert-error">
								<button type="button" class="close" data-dismiss="alert">x</button>
								<h4>Peringatan</h4>
								<p>Peringatan</p>
							</div>
							<?php }else{foreach($data_get as $dg){?>
								<tr class="warning">
									<td><?php echo $no++;?></td>
									<td><?php echo $dg->nama_jemaat;?></td>
									<td><?php echo $dg->jenis_kelamin;?></td>
									<td><?php echo $dg->tgl_lahir;?></td>
									<td><?php echo $dg->alamat_jemaat;?></td>
									<td><?php echo $dg->tlp_jemaat;?></td>
									<td><?php echo $dg->bbm_wa;?></td>
									<td><a href="<?php echo base_url('index.php/dashboard/aktivasi/');?>/<?php echo $dg->id_kfc; ?>" onClick="return confirm('Aktifkan <?php echo $dg->nama_jemaat;?> ?..')"><span class="glyphicon glyphicon-pencil"></span></a></td>
				
								</tr>
							<?php	}	}?>
							<tbody>
					</table>
				</section>
			</div>
		</div>
	</div>
<!--isi web-->
<!--/div>
<!--end body-->