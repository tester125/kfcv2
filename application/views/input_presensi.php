<!--body-->
<div class="container">

	<div class="row">
		<?php $atribut=array('class'=>'well');
		echo form_open('dashboard/input_presensi',$atribut);?>
		<div class="col-md-12">
			<h1>Presensi</h1><hr>
		</div>
		
		<div class="col-md-4">
		<div class="input-group">
        <span class="input-group-addon"><i class="glyphicon glyphicon-list"></i></span>
		 <label class="form-control">Tanggal Presensi</label>
		<input type="date" name="tgl_presensi" class="form-control" placeholder="">
		</div>
		</div>
		
		<table width="100%" class="table table-condensed table-hover" >
			<thead >
				<td>No.</td>
				<td width="60%">Nama</td>
				<td width="20%">Tanggal Lahir</td>
				<td>Kehadiran</td>
			</thead>
			<tbody>
			<?php
								$no=1;
								if($data_get==NULL){?>
							<div class="alert alert-error">
								<button type="button" class="close" data-dismiss="alert">x</button>
								<h4>Peringatan</h4>
								<p>Peringatan</p>
							</div>
							<?php }else{foreach($data_get as $dg){?>
								<tr >
									<td><?php echo $no++;?></td>
									<input type="hidden" name="id_kfc[]" id="id_kfc[]" value="<?php echo $dg->id_kfc;?>">
									<input type="hidden" name="absen[]" id="absen[]" value="<?php echo $dg->absen;?>">
									<input type="hidden" name="status[]" id="status[]" value="<?php echo $dg->status;?>">
									<td><input type="hidden" name="nama_jemaat[]" value="<?php echo $dg->nama_jemaat;?>"><?php echo $dg->nama_jemaat;?></td>
									<td><input type="hidden" name="tgl_lahir[]" value="<?php echo $dg->tgl_lahir;?>"><?php echo $dg->tgl_lahir;?></td>
									<td><select name="presensi[]">
										<option value="0" selected>Tidak Hadir</option>
										<option value="1">Hadir</option>
										</select></td>
								</tr>
							<?php	}	}?>
			</tbody>
		</table>
		<div class="form-group">
		  
		  
			<button type="submit" class="btn btn-warning" value="submit" name="submit">Send <span class="glyphicon glyphicon-send"></span></button>
		  
		</div>
		</form>
	</div>
    
</div>
    </div><!-- /.container -->
<!--end body-->