 
 <div class="main-panel" >
            <nav class="navbar navbar-transparent navbar-absolute">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#"> Dashboard </a>
                    </div>
                </div>
            </nav>
            <div class="content">
                <div class="container-fluid">
                    
					<div class="row">
                        <div class="col-lg-4 col-md-6 col-sm-6">
                            <div class="card card-stats">
                                <div class="card-header" data-background-color="orange">
                                    <i class="material-icons">accessibility</i>
                                </div>
                                <div class="card-content">
                                    <p class="category">Jemaat</p>
                                    <h3 class="title"><?php echo $total_jemaat_aktif."/".$total_jemaat;?>
                                        <small>Orang</small>
                                    </h3>
                                </div>
                                <div class="card-footer">
                                    <div class="stats">
                                        <i class="material-icons text-danger">warning</i>
                                        <a href="<?php echo base_url("/index.php/dashboard02/jemaat");?>">klik untuk info lebih lanjut...</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-6">
                            <div class="card card-stats">
                                <div class="card-header" data-background-color="green">
                                    <i class="material-icons">party_mode</i>
                                </div>
                                <div class="card-content">
                                    <p class="category">Jemaat Hadir</p>
                                    <h3 class="title"><?php echo $total_presensi+$total_jemaat_baru;?></h3>
                                </div>
                                <div class="card-footer">
                                    <div class="stats">
                                        <i class="material-icons">date_range</i> di tanggal <?php echo date("d-m-Y");?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-6">
                            <div class="card card-stats">
                                <div class="card-header" data-background-color="red">
                                    <i class="material-icons"><?php if($sekarang<$awal){
																				echo"trending_down";}else{
																					if($sekarang>$awal){
																						echo"trending_up";
																					}else{echo "timeline";}
																				}?></i>
                                </div>
                                <div class="card-content">
                                    <p class="category">Progres Presensi</p>
                                    <h3 class="title"><?php if($awal==0){echo "100%";}else{echo round($sekarang/$awal*100,2)."%";}?></h3>
                                </div>
                                <div class="card-footer">
                                    <div class="stats">
                                        <i class="material-icons">local_offer</i> Dari minggu lalu <?php echo $awal;?> orang
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--RENCANA UNTUK INDIKATOR SOSMED-->
                        <!--div class="col-lg-3 col-md-6 col-sm-6">
                            <div class="card card-stats">
                                <div class="card-header" data-background-color="blue">
                                    <i class="fa fa-twitter"></i>
                                </div>
                                <div class="card-content">
                                    <p class="category">####</p>
                                    <h3 class="title">####</h3>
                                </div>
                                <div class="card-footer">
                                    <div class="stats">
                                        <i class="material-icons">update</i> ####
                                    </div>
                                </div>
                            </div>
                        </div-->
                    </div>
			<div class="row">
						<div class="col-lg-4 col-md-12">
                            <div class="card">
                                <div class="card-header" data-background-color="blue">
                                    <h4 class="title">Pembina</h4>
                                    <p class="category">Jemaat Baru di <?php echo date("Y-m-d");?></p>
                                </div>
                                <div class="card-content table-responsive">
                                    <table class="table table-hover">
                                        <thead class="text-warning">
                                            <th>No</th>
                                            <th>Name</th>
                                            <th>Kelamin</th>
                                            <th>Status</th>
                                        </thead>
                                        <tbody>
										<?php
								$no=1;
								if($data_get==NULL){?>
							<div class="alert alert-error">
								<button type="button" class="close" data-dismiss="alert">x</button>
								<h4>Peringatan</h4>
								<p>Peringatan</p>
							</div>
							<?php }else{foreach($data_get as $dg){?>
							<tr >
									<td><?php echo $no++;?></td>
									<td><?php echo $dg->nama_jemaat;?></td>
									<td><?php echo $dg->jenis_kelamin;?></td>
									<td><?php if($dg->tgl_masuk==date("Y-m-d")){echo "Baru";}else{echo "Lama";}?></td>
									</tr>
									<?php	}	}?>
                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
					
                        <div class="col-lg-4 col-md-12">
                            <div class="card">
                                <div class="card-header" data-background-color="orange">
                                    <h4 class="title">Suspect</h4>
                                    <p class="category">Presensi <?php echo date("Y-m-d");?></p>
                                </div>
                                <div class="card-content table-responsive">
                                    <table class="table table-hover">
                                        <thead class="text-warning">
                                            <th>No</th>
                                            <th>Name</th>
                                            <th>Gender</th>
                                            <th>Jam Hadir</th>
                                        </thead>
                                        <tbody>
										<?php
								$no=1;
								if($data_presensi==NULL){?>
							<div class="alert alert-error">
								<button type="button" class="close" data-dismiss="alert">x</button>
								<h4>Peringatan</h4>
								<p>Peringatan</p>
							</div>
							<?php }else{foreach($data_presensi as $dg){?>
							<tr >
									<td><?php echo $no++;?></td>
									<td><?php echo $dg->nama_jemaat;?></td>
									<td><?php echo $dg->jenis_kelamin;?></td>
									<td><?php echo $dg->jam_presensi;?></td>
									</tr>
									<?php	}	}?>
                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
						<div class="col-lg-4 col-md-12">
                            <div class="card">
                                <div class="card-header" data-background-color="orange">
                                    <h4 class="title">Deal</h4>
                                    <p class="category">Presensi <?php echo date("Y-m-d");?></p>
                                </div>
                                <div class="card-content table-responsive">
                                    <table class="table table-hover">
                                        <thead class="text-warning">
                                            <th>No</th>
                                            <th>Name</th>
                                            <th>Gender</th>
                                            <th>Tgl Lahir</th>
											<th>Status</th>
                                        </thead>
                                        <tbody>
										<?php
								$no=1;
								if($data_ultah==NULL){?>
							<div class="alert alert-error">
								<button type="button" class="close" data-dismiss="alert">x</button>
								<h4>Peringatan</h4>
								<p>Peringatan</p>
							</div>
							<?php }else{foreach($data_ultah as $dg){?>
							<tr >
									<td><?php echo $no++;?></td>
									<td><?php echo $dg->nama_jemaat;?></td>
									<td><?php echo $dg->jenis_kelamin;?></td>
									<td><?php echo $dg->tgl_lahir;?></td>
									<td><?php if($dg->status==1){
										echo "Aktif";
										}else{
											echo "Tidak Aktif";
											}?></td>
									</tr>
									<?php	}	}?>
                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--rencana panel info visitasi, kuantitas dll-->
					<!--div class="row">
					    <div class="col-lg-4 col-md-12">
                            <div class="card">
                                <div class="card-header" data-background-color="red">
                                    <h4 class="title">Visitasi</h4>
                                    <p class="category"></p>
                                </div>
                                <div class="card-content table-responsive">
                                    <table class="table table-hover">
                                        <thead class="text-warning">
                                            <th>No</th>
                                            <th>Name</th>
                                            <th>Gender</th>
											<th>Qty Absen(3bln)</th>
                                        </thead>
                                        <tbody>
										<?php
								$no=1;
								if($data_absen==NULL){?>
							<div class="alert alert-error">
								<button type="button" class="close" data-dismiss="alert">x</button>
								<h4>Peringatan</h4>
								<p>Peringatan</p>
							</div>
							<?php }else{foreach($data_absen as $dg){?>
							<tr >
									<td><?php echo $no++;?></td>
									<td><?php echo $dg->nama_jemaat;?></td>
									<td><?php echo $dg->jenis_kelamin;?></td>
									<td><?php echo $dg->absen;?></td>
									</tr>
									<?php	}	}?>
                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
					    <div class="col-lg-4 col-md-12">
                            <div class="card">
                                <div class="card-header" data-background-color="blue">
                                    <h4 class="title">Perkembangan Jemaat per Minggu</h4>
                                    <p class="category"></p>
                                </div>
                                <div class="card-content table-responsive">
                                    <table class="table table-hover">
                                        <thead class="text-warning">
                                            <th>Tgl</th>
                                            <th>Total</th>
                                            
                                        </thead>
                                        <tbody>
										<?php
								$no=1;
								
								foreach($kenaikan as $dg){?>
							<tr >
									
									<td><p hidden ><?php echo $no++;?></p><?php echo $dg->tot;?></td>
									<td><?php echo $dg->jml;?></td>
									
									</tr>
									<?php		}?>
                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-12">
                            <div class="card">
                                <div class="card-header" data-background-color="blue">
                                    <h4 class="title">Jemaat Setia</h4>
                                    <p class="category">Dalam 3 Bulan Terakhir</p>
                                </div>
                                <div class="card-content table-responsive">
                                    <table class="table table-hover">
                                        <thead class="text-warning">
                                            <th>ID</th>
                                            <th>Name</th>
                                            <th>Ranking</th>
                                            
                                        </thead>
                                        <tbody>
										<?php
								$no=1;
								if($data_ranking==NULL){?>
							<div class="alert alert-error">
								<button type="button" class="close" data-dismiss="alert">x</button>
								<h4>Peringatan</h4>
								<p>Peringatan</p>
							</div>
							<?php }else{foreach($data_ranking as $dg){?>
							<tr >
									<td><?php echo $no++;?></td>
									<td><?php echo $dg->nama_jemaat;?></td>
									<td><?php echo $dg->jml;?></td>
									
									</tr>
									<?php	}	}?>
                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        
                    </div-->
					
                </div>
            </div>
			<?php
$url=$_SERVER['REQUEST_URI'];
header("Refresh: 30; URL=$url"); 
?>