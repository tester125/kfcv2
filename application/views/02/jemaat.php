<div class="main-panel">
            <nav class="navbar navbar-transparent navbar-absolute">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#"> Data Jemaat </a>
                    </div>
                    <div class="collapse navbar-collapse">
                        <ul class="nav navbar-nav navbar-right">
                            <li>
                                <a href="#pablo" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="material-icons">dashboard</i>
                                    <p class="hidden-lg hidden-md">Dashboard</p>
                                </a>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="material-icons">notifications</i>
                                    <span class="notification">5</span>
                                    <p class="hidden-lg hidden-md">Notifications</p>
                                </a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <a href="#">Mike John responded to your email</a>
                                    </li>
                                    <li>
                                        <a href="#">You have 5 new tasks</a>
                                    </li>
                                    <li>
                                        <a href="#">You're now friend with Andrew</a>
                                    </li>
                                    <li>
                                        <a href="#">Another Notification</a>
                                    </li>
                                    <li>
                                        <a href="#">Another One</a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="#pablo" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="material-icons">person</i>
                                    <p class="hidden-lg hidden-md">Profile</p>
                                </a>
                            </li>
                        </ul>
                        <form class="navbar-form navbar-right" role="search">
                            <div class="form-group  is-empty">
                                <input type="text" class="form-control" placeholder="Search">
                                <span class="material-input"></span>
                            </div>
                            <button type="submit" class="btn btn-white btn-round btn-just-icon">
                                <i class="material-icons">search</i>
                                <div class="ripple-container"></div>
                            </button>
                        </form>
                    </div>
                </div>
            </nav>
            <div class="content">
                <div class="container-fluid">
					<div class="row">
						<div class="col-lg-12 col-md-12">
                            <div class="card card-nav-tabs">
                                <div class="card-header" data-background-color="blue">
                                    <div class="nav-tabs-navigation">
                                        <div class="nav-tabs-wrapper">
                                            
                                            <ul class="nav nav-tabs" data-tabs="tabs">
                                                <li class="active">
                                                    <a href="#profile" data-toggle="tab">
                                                        <i class="material-icons">people</i> Data Jemaat/Anggota
                                                        <div class="ripple-container" ></div>
                                                    </a>
                                                </li>
                                                <li class="">
                                                    <a href="#messages" data-toggle="tab">
                                                        <i class="material-icons">notifications</i> Ulang Tahun
                                                        <div class="ripple-container"></div>
                                                    </a>
                                                </li>												
                                                <li class="">
                                                    <a href="<?php echo base_url("/index.php/dashboard02/excel");?>" >
                                                        <i class="material-icons">archive</i> Export Jemaat to Excel
                                                        <div class="ripple-container"></div>
                                                    </a>
                                                </li>
												<li class=" pull-right">
                                                    <a href="#settings" data-toggle="tab">
                                                        <i class="material-icons">note_add</i> Tambah Jemaat
                                                        <div class="ripple-container"></div>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-content">
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="profile">
                                            <table class="table">
												<thead class="text-primary">
                                            <th>No</th>
                                            <th>Id</th>
                                            <th>Nama</th>
                                            <th>Gender</th>
											<th>Tlp</th>
                                            <th>Tgl Lahir</th>
											<th>Tgl Masuk</th>
											<th>Action</th>
											
											</thead>
                                                <tbody>
												
												<?php
												$no=1;
												if($data_get==NULL){?>
												<div class="alert alert-error">
												<button type="button" class="close" data-dismiss="alert">x</button>
												<h4>Peringatan</h4>
												<p>Peringatan</p>
												</div>
												<?php }else{foreach($data_get as $dg){?>
												
                                                    <tr>
														<td><?php echo $no++;?></td>
                                                        <td><?php echo $dg->id_kfc;?></td>
														<td><a href="<?php echo base_url('index.php/dashboard02/profil');?>/<?php echo $dg->id_kfc; ?>"><?php echo $dg->nama_jemaat;?></a></td>
														<td><?php echo $dg->jenis_kelamin;?></td>
														<td><?php echo $dg->tlp_jemaat;?></td>
														<td><?php echo $dg->tgl_lahir;?></td>
														<td><?php echo $dg->tgl_masuk;?></td>
														<td class="td-actions text-right" >
                                                            
															<a href="<?php echo base_url('index.php/dashboard02/edit');?>/<?php echo $dg->id_kfc; ?>" rel="tooltip" title="Edit Task" class="btn btn-primary btn-simple btn-xs"><i class="material-icons">edit</i></a>
                                                            
															<a href="<?php echo base_url('index.php/dashboard02/hapus');?>/<?php echo $dg->id_kfc; ?>" onClick="return confirm('Yakin Hapus <?php echo $dg->nama_jemaat;?> ?..')" rel="tooltip" title="Remove" class="btn btn-danger btn-simple btn-xs"><i class="material-icons">close</i></a>
                                                        </td>
                                                    </tr>
													<?php	}	}?>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="tab-pane" id="messages">
                                            <table class="table">
                                                <tbody>
												<?php
												$no=1;
												if($data_ultah==NULL){?>
												<div class="alert alert-error">
												<button type="button" class="close" data-dismiss="alert">x</button>
												<h4>Peringatan</h4>
												<p>Peringatan</p>
												</div>
												<?php }else{foreach($data_ultah as $dg){?>
                                                    <tr>
													<td><?php echo $no++;?></td>
									<td><a href="https://api.whatsapp.com/send?phone=62<?php echo ltrim(($dg->tlp_jemaat),'0');?>&text=Happy Birthday"><?php echo $dg->nama_jemaat;?></a></td>
									<td><?php echo $dg->jenis_kelamin;?></td>
									<td><?php echo $dg->tgl_lahir;?></td>
									<td><?php if($dg->status==1){
										echo "Aktif";
										}else{
											echo "Tidak Aktif";
											}?></td>
									<td class="td-actions text-right">
                                                            <button type="button" rel="tooltip" title="Edit Task" class="btn btn-primary btn-simple btn-xs">
                                                                <i class="material-icons">edit</i>
                                                            </button>
                                                            <button type="button" rel="tooltip" title="Remove" class="btn btn-danger btn-simple btn-xs">
                                                                <i class="material-icons">close</i>
                                                            </button>
                                                        </td>
									</tr>
									<?php	}	}?>                                                            
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="tab-pane" id="settings">
                                         <!-- form tambah jemaaat-->   
                                    <form action="<?php echo base_url('index.php/dashboard02/insert_jemaat');?>" method="POST">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group label-floating">
													<i class="material-icons">peoples</i>
                                                    <label class="control-label"><?php echo $id_kfc;?></label>
                                                    
													<input name="id_kfc" class="form-control" type="HIDDEN" value="<?php echo $id_kfc;?>">
                                                </div>
                                            </div>
                                            
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group label-floating">
                                                    <label class="control-label">Fist Name</label>
													<input type="text" name="nama" class="form-control" autofocus required="required">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group label-floating">
                                                    <label class="control-label">Adress</label>
                                                    <input name="alamat"  class="form-control" type="text">
                                                </div>
                                            </div>
                                        </div>
										<div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group label-floating">
                                                    <label class="control-label">Gender</label>
                                                    <input type="radio" name="jenis_kelamin" value="L" selected /> Laki-laki
													<input type="radio" name="jenis_kelamin" value="P" /> Perempuan
                                                </div>
                                            </div>
											<div class="col-md-4">
                                                <div class="form-group label-floating">
                                                    <label class="control-label"></label>
                                                    Birthday(Tgl Lahir)<input name="tgl_lahir"  class="form-control"  type="date">
                                                </div>
                                            </div>
											<div class="col-md-4">
                                                <div class="form-group label-floating">
                                                    <label class="control-label"></label>
                                                    Join date<input name="tgl_masuk" placeholder="Join date" class="form-control"  type="date">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group label-floating">
                                                    <label class="control-label">Pin BB/WhatApp</label>
                                                    <input name="bbm_wa"  class="form-control" type="text">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group label-floating">
                                                    <label class="control-label">Tlp</label>
                                                    <input name="tlp"  class="form-control" type="text">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group label-floating">
                                                    <label class="control-label">Facebook / Instagram</label>
                                                    <input name="fb_ig"  class="form-control" type="text">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>About Me</label>
                                                    <div class="form-group label-floating">
                                                        <label class="control-label"> Thanks for join us, Selamat bergabung dengan kami di Youth Party GBI GAMA.</label>
                                                        <textarea class="form-control" rows="5"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <button type="submit" value="submit" name="submit" class="btn btn-primary pull-right">Update Profile</button>
										
                                        <div class="clearfix"></div>
                                    </form>
                                
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
					</div>
                    
                </div>
            </div>