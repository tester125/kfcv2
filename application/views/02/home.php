 
 <div class="main-panel" >
            <nav class="navbar navbar-transparent navbar-absolute">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#"> Dashboard </a>
                    </div>
                    <!--div class="collapse navbar-collapse">
                        <ul class="nav navbar-nav navbar-right">
                            <li>
                                <a href="<?php echo base_url();?>" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="material-icons">dashboard</i>
                                    <p class="hidden-lg hidden-md">Dashboard</p>
                                </a>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="material-icons">notifications</i>
                                    <span class="notification">5</span>
                                    <p class="hidden-lg hidden-md">Notifications</p>
                                </a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <a href="#">Mike John responded to your email</a>
                                    </li>
                                    <li>
                                        <a href="#">You have 5 new tasks</a>
                                    </li>
                                    <li>
                                        <a href="#">You're now friend with Andrew</a>
                                    </li>
                                    <li>
                                        <a href="#">Another Notification</a>
                                    </li>
                                    <li>
                                        <a href="#">Another One</a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="#pablo" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="material-icons">person</i>
                                    <p class="hidden-lg hidden-md">Profile</p>
                                </a>
                            </li>
                        </ul>
                        <form class="navbar-form navbar-right" role="search">
                            <div class="form-group  is-empty">
                                <input type="text" class="form-control" placeholder="Search">
                                <span class="material-input"></span>
                            </div>
                            <button type="submit" class="btn btn-white btn-round btn-just-icon">
                                <i class="material-icons">search</i>
                                <div class="ripple-container"></div>
                            </button>
                        </form>
                    </div-->
                </div>
            </nav>
            <div class="content">
                <div class="container-fluid">
                    <div id="notifications"><?php echo $this->session->flashdata('msg'); ?></div>
					
			<div class="row">
						<div class="col-lg-6 col-md-12">
                            <div class="card">
                                <div class="card-header" data-background-color="blue">
                                    <h4 class="title">Jemaat Datang Pertama Kali</h4>
                                    <p class="category">Jemaat Baru di <?php echo date("Y-m-d");?></p>
                                </div>
                                <div class="card-content table-responsive">
                                    <table class="table table-hover">
                                        <thead class="text-warning">
                                            <th>No</th>
                                            <th>Name</th>
                                            <th>Kelamin</th>
                                            <th>tgl gabung</th>
                                            <th>Follow Up</th>
                                        </thead>
                                        <tbody>
										<?php
								$no=1;
								if($data_get==NULL){?>
							<div class="alert alert-error">
								<button type="button" class="close" data-dismiss="alert">x</button>
								<h4>Peringatan</h4>
								<p>Peringatan</p>
							</div>
							<?php }else{foreach($data_get as $dg){?>
							<tr >
									<td><?php echo $no++;?></td>
									<td><a href="<?php echo base_url('index.php/dashboard02/fu');?>/<?php echo $dg->id_kfc; ?>"><?php echo $dg->nama_jemaat;?></a></td>
									<td><?php echo $dg->jenis_kelamin;?></td>
                                    <td><?php echo $dg->tgl_masuk_fu;?></td>
									<td><?php if($dg->tgl_masuk==date("Y-m-d")){echo "Baru";}else{echo "Lama";}?></td>
									</tr>
									<?php	}	}?>
                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
            </div>
				<div class="row">	
                        <div class="col-lg-3 col-md-12">
                            <div class="card">
                                <div class="card-header" data-background-color="red">
                                    <h4 class="title">Follow Up Tahap 1</h4>
                                    <p class="category">Presensi <?php echo date("Y-m-d");?></p>
                                </div>
                                <div class="card-content table-responsive">
                                    <table class="table table-hover">
                                        <thead class="text-warning">
                                            <th>No</th>
                                            <th>Name</th>
                                            <th>FU</th>
                                        </thead>
                                        <tbody>
										<?php
                                $no=1;
                                if($data_get==NULL){?>
                            <div class="alert alert-error">
                                <button type="button" class="close" data-dismiss="alert">x</button>
                                <h4>Peringatan</h4>
                                <p>Peringatan</p>
                            </div>
                            <?php }else{foreach($data_get as $dg){
                                if($dg->goal==null){
                                if($dg->iii==null){
                                    if($dg->ii==null){
                                        if($dg->i==null){}else{
                                    echo "<tr >
                                    <td>".$no++."</td>
                                    <td><a href='".base_url('index.php/dashboard02/fu')."/".$dg->id_kfc."'>".$dg->nama_jemaat."/".$dg->id_kfc."</a></td>
                                    <td>".$dg->fu."</td>
                                    <td></td>
                                    </tr>";}
                                    }
                                }}
                            }   }?>
                            
                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
						<div class="col-lg-3 col-md-12">
                            <div class="card">
                                <div class="card-header" data-background-color="orange">
                                    <h4 class="title">Follow Up Tahap 2</h4>
                                    <p class="category">Presensi <?php echo date("Y-m-d");?></p>
                                </div>
                                <div class="card-content table-responsive">
                                    <table class="table table-hover">
                                        <thead class="text-warning">
                                            <th>No</th>
                                            <th>Name</th>
                                            <th>FU</th>
                                        </thead>
                                        <tbody>
										<?php
                                $no=1;
                                if($data_get==NULL){?>
                            <div class="alert alert-error">
                                <button type="button" class="close" data-dismiss="alert">x</button>
                                <h4>Peringatan</h4>
                                <p>Peringatan</p>
                            </div>
                            <?php }else{foreach($data_get as $dg){
                                if($dg->goal==null){
                                if($dg->iii==null){
                                    if($dg->ii==null){
                                        if($dg->i==null){}
                                    }else{echo "<tr >
                                    <td>".$no++."</td>
                                    <td><a href='".base_url('index.php/dashboard02/fu')."/".$dg->id_kfc."'>".$dg->nama_jemaat."/".$dg->id_kfc."</a></td>
                                    <td>".$dg->fu."</td>
                                    <td></td>
                                    </tr>";}
                                }}
                            }   }?>
                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-12">
                            <div class="card">
                                <div class="card-header" data-background-color="green">
                                    <h4 class="title">Follow Up Tahap 3</h4>
                                    <p class="category">Presensi <?php echo date("Y-m-d");?></p>
                                </div>
                                <div class="card-content table-responsive">
                                    <table class="table table-hover">
                                        <thead class="text-warning">
                                            <th>No</th>
                                            <th>Name</th>
                                            <th>FU</th>
                                        </thead>
                                        <tbody>
                                        <?php
                                $no=1;
                                if($data_get==NULL){?>
                            <div class="alert alert-error">
                                <button type="button" class="close" data-dismiss="alert">x</button>
                                <h4>Peringatan</h4>
                                <p>Peringatan</p>
                            </div>
                            <?php }else{foreach($data_get as $dg){
                                if($dg->goal==null){
                                if($dg->iii==null){
                                    if($dg->ii==null){
                                        if($dg->i==null){}
                                    }
                                }else{echo "<tr >
                                    <td>".$no++."</td>
                                    <td><a href='".base_url('index.php/dashboard02/fu')."/".$dg->id_kfc."'>".$dg->nama_jemaat."/".$dg->id_kfc."</a></td>
                                    <td>".$dg->fu."</td>
                                    <td></td>
                                    </tr>";}}
                            }   }?>
                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    
                    <div class="col-lg-3 col-md-12">
                            <div class="card">
                                <div class="card-header" data-background-color="blue">
                                    <h4 class="title">Goal</h4>
                                    <p class="category">Presensi <?php echo date("Y-m-d");?></p>
                                </div>
                                <div class="card-content table-responsive">
                                    <table class="table table-hover">
                                        <thead class="text-warning">
                                            <th>No</th>
                                            <th>Name</th>
                                            <th>Goal</th>
                                        </thead>
                                        <tbody>
                                        <?php
                                $no=1;
                                if($data_get==NULL){?>
                            <div class="alert alert-error">
                                <button type="button" class="close" data-dismiss="alert">x</button>
                                <h4>Peringatan</h4>
                                <p>Peringatan</p>
                            </div>
                            <?php }else{foreach($data_get as $dg){
                                if($dg->goal==null){
                                    }else{echo "<tr >
                                    <td>".$no++."</td>
                                    <td><a href='".base_url('index.php/dashboard02/fu')."/".$dg->id_kfc."'>".$dg->nama_jemaat."/".$dg->id_kfc."</a></td>
                                    <td>".$dg->goal."</td>
                                    <td></td>
                                    </tr>";}
                            }   }?>
                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div></div>
                    <!--rencana panel info visitasi, kuantitas dll-->
					<!--div class="row">
					    <div class="col-lg-4 col-md-12">
                            <div class="card">
                                <div class="card-header" data-background-color="red">
                                    <h4 class="title">Visitasi</h4>
                                    <p class="category"></p>
                                </div>
                                <div class="card-content table-responsive">
                                    <table class="table table-hover">
                                        <thead class="text-warning">
                                            <th>No</th>
                                            <th>Name</th>
                                            <th>Gender</th>
											<th>Qty Absen(3bln)</th>
                                        </thead>
                                        <tbody>
										<?php
								$no=1;
								if($data_absen==NULL){?>
							<div class="alert alert-error">
								<button type="button" class="close" data-dismiss="alert">x</button>
								<h4>Peringatan</h4>
								<p>Peringatan</p>
							</div>
							<?php }else{foreach($data_absen as $dg){?>
							<tr >
									<td><?php echo $no++;?></td>
									<td><?php echo $dg->nama_jemaat;?></td>
									<td><?php echo $dg->jenis_kelamin;?></td>
									<td><?php echo $dg->absen;?></td>
									</tr>
									<?php	}	}?>
                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
					    <div class="col-lg-4 col-md-12">
                            <div class="card">
                                <div class="card-header" data-background-color="blue">
                                    <h4 class="title">Perkembangan Jemaat per Minggu</h4>
                                    <p class="category"></p>
                                </div>
                                <div class="card-content table-responsive">
                                    <table class="table table-hover">
                                        <thead class="text-warning">
                                            <th>Tgl</th>
                                            <th>Total</th>
                                            
                                        </thead>
                                        <tbody>
										<?php
								$no=1;
								
								foreach($kenaikan as $dg){?>
							<tr >
									
									<td><p hidden ><?php echo $no++;?></p><?php echo $dg->tot;?></td>
									<td><?php echo $dg->jml;?></td>
									
									</tr>
									<?php		}?>
                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-12">
                            <div class="card">
                                <div class="card-header" data-background-color="blue">
                                    <h4 class="title">Jemaat Setia</h4>
                                    <p class="category">Dalam 3 Bulan Terakhir</p>
                                </div>
                                <div class="card-content table-responsive">
                                    <table class="table table-hover">
                                        <thead class="text-warning">
                                            <th>ID</th>
                                            <th>Name</th>
                                            <th>Ranking</th>
                                            
                                        </thead>
                                        <tbody>
										<?php
								$no=1;
								if($data_ranking==NULL){?>
							<div class="alert alert-error">
								<button type="button" class="close" data-dismiss="alert">x</button>
								<h4>Peringatan</h4>
								<p>Peringatan</p>
							</div>
							<?php }else{foreach($data_ranking as $dg){?>
							<tr >
									<td><?php echo $no++;?></td>
									<td><?php echo $dg->nama_jemaat;?></td>
									<td><?php echo $dg->jml;?></td>
									
									</tr>
									<?php	}	}?>
                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        
                    </div-->
					
                </div>
            </div>
			<?php
//$url=$_SERVER['REQUEST_URI'];
//header("Refresh: 30; URL=$url"); 
?>