<div class="main-panel">
            <nav class="navbar navbar-transparent navbar-absolute">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#"> Edit-Profil </a>
                    </div>
                    <div class="collapse navbar-collapse">
                        <ul class="nav navbar-nav navbar-right">
                            <li>
                                <a href="#pablo" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="material-icons">dashboard</i>
                                    <p class="hidden-lg hidden-md">Dashboard</p>
                                </a>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="material-icons">notifications</i>
                                    <span class="notification">5</span>
                                    <p class="hidden-lg hidden-md">Notifications</p>
                                </a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <a href="#">Mike John responded to your email</a>
                                    </li>
                                    <li>
                                        <a href="#">You have 5 new tasks</a>
                                    </li>
                                    <li>
                                        <a href="#">You're now friend with Andrew</a>
                                    </li>
                                    <li>
                                        <a href="#">Another Notification</a>
                                    </li>
                                    <li>
                                        <a href="#">Another One</a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="#pablo" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="material-icons">person</i>
                                    <p class="hidden-lg hidden-md">Profile</p>
                                </a>
                            </li>
                        </ul>
                        <form class="navbar-form navbar-right" role="search">
                            <div class="form-group  is-empty">
                                <input type="text" class="form-control" placeholder="Search">
                                <span class="material-input"></span>
                            </div>
                            <button type="submit" class="btn btn-white btn-round btn-just-icon">
                                <i class="material-icons">search</i>
                                <div class="ripple-container"></div>
                            </button>
                        </form>
                    </div>
                </div>
            </nav>
            <div class="content">
                <div class="container-fluid">
					
                    <div class="row">
                        
						<div class="col-md-4">
                            <!--mulai edit-->
                            <div class="card card-profile">
     
                               

                                <div class="card-avatar " data-background-color="blue">
                                    <!--h4 class="title"><?php foreach($data_profil as $data){ echo $data->nama_jemaat;}?>Data Presensi</h4-->
                                    <img class="img" src="<?php 
                                    echo base_url();?>foto/<?php foreach($data_profil as $data){ 
                                        
                                        if($data->foto==null){
                                            echo "user.jpg";
                                        }else{echo $data->foto;}
                                    }
                                        ?>" width="100px">
                                    <div class="row">
                                        <!--div class="col-md-4">
                                            <p>fotonya <?php //foreach($data_profil as $data){ echo $data->nama_jemaat;}?></p><br>
                                            
                                            </div>
                                        <div class="col-md-7">
                                            <p>fotonya <?php //foreach($data_profil as $data){ echo $data->id_kfc;}?></p>
                                                    
                                        </div-->

                                    </div>
									

                                </div>

                                <div class="card-content card-body ">
                                    <h6 class="card-category text-gray">CEO / Co-Founder</h6>
                                    <h4 class="card-title"><?php foreach($data_profil as $data){ echo $data->nama_jemaat;}?></h4>
                                    <p class="card-description">
                    Don't be scared of the truth because we need to restart the human foundation in truth And I love you like Kanye loves Kanye I love Rick Owens’ bed design but the back is...
                  </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="card card-nav-tabs">
                                <div class="card-header" data-background-color="green">
                                    <h4><?php foreach($data_profil as $data){ echo $data->id_kfc;}?></h4>
                                </div>
                                <div class="card-content">
                                    <div class="tab-content">
                                        <div class="tab-pane active">

                            <?php echo form_open("dashboard02/edit", array('enctype'=>'multipart/form-data')); ?>
                    <!--nama-->     <div class="form-group label-floating is-empty">
                                        <label class="control-label"></label>
                                        <input type="TEXT" class="form-control" name="nama_jemaat" value="<?php foreach($data_profil as $data){ echo $data->nama_jemaat;}?>"><?php foreach($data_profil as $data){
                                            if($data->nama_jemaat==null){}else{echo "(Nama)";}
                                        }?>
                                        <input type="hidden" class="form-control" name="id_kfc" value="<?php foreach($data_profil as $data){ echo $data->id_kfc;}?>">
                                        <span class="material-input"></span>
                                    </div>
                   <!--tgl lahir-->  <div class="form-group label-floating is-empty">
                                        <label class="control-label "><?php foreach($data_profil as $data){
                                            if($data->tgl_lahir==null){echo "alamat";}
                                        }?></label>
                                        <input type="date" class="form-control" name="tgl_lahir" value="<?php foreach($data_profil as $data){ echo $data->tgl_lahir;}?>" required>
                                        <?php foreach($data_profil as $data){
                                            if($data->tgl_lahir==null){}else{echo "(Alamat)";}
                                        }?>
                                        <span class="material-input"></span>
                                    </div>
                    <!--alamat-->     <div class="form-group label-floating is-empty">
                                        <label class="control-label "><?php foreach($data_profil as $data){
                                            if($data->alamat_jemaat==null){echo "alamat";}
                                        }?></label>
                                        <input type="TEXT" class="form-control" name="alamat_jemaat" value="<?php foreach($data_profil as $data){ echo $data->alamat_jemaat;}?>" required>
                                        <?php foreach($data_profil as $data){
                                            if($data->alamat_jemaat==null){}else{echo "(Alamat)";}
                                        }?>
                                        <span class="material-input"></span>
                                    </div>
                    <!--tlp-->     <div class="form-group label-floating is-empty">
                                        <label class="control-label "><?php foreach($data_profil as $data){
                                            if($data->tlp_jemaat==null){echo "Telepon";}
                                        }?></label>
                                        <input type="TEXT" class="form-control" name="tlp_jemaat" value="<?php foreach($data_profil as $data){ echo $data->tlp_jemaat;}?>" required>
                                        <?php foreach($data_profil as $data){
                                            if($data->tlp_jemaat==null){}else{echo "(Telepon)";}
                                        }?>
                                        <span class="material-input"></span>
                                    </div>
                    <!--wa-->     <div class="form-group label-floating is-empty">
                                        <label class="control-label "><?php foreach($data_profil as $data){
                                            if($data->bbm_wa==null){echo "Whatsapp";}
                                        }?></label>
                                        <input type="TEXT" class="form-control" name="bbm_wa" value="<?php foreach($data_profil as $data){ echo $data->bbm_wa;}?>"><?php foreach($data_profil as $data){
                                            if($data->bbm_wa==null){}else{echo "(Whatsapp)";}
                                        }?>
                                        <span class="material-input"></span>
                                    </div>
                    <!--komsel-->     <div class="form-group label-floating is-empty">
                                        <label class="control-label "><?php foreach($data_profil as $data){
                                            if($data->komsel==null){echo "komsel";}
                                        }?></label>
                                        <input type="TEXT" class="form-control" name="komsel" value="<?php foreach($data_profil as $data){ echo $data->komsel;}?>"><?php foreach($data_profil as $data){
                                            if($data->komsel==null){}else{echo "(komsel)";}
                                        }?>
                                        <span class="material-input"></span>
                                    </div>
                     <!--gender-->     <div class="form-group label-floating is-empty">
                                        <label class="control-label "><?php foreach($data_profil as $data){
                                            if($data->jenis_kelamin==null){echo "Whatsapp";}
                                        }?></label>
                                        <select  class="form-control" name="jenis_kelamin" required>
                                            <option value="">No Selected</option>
                                            <option value="L">Laki-laki</option>
                                            <option value="P">Perempuan</option>
                                        </select>
                                        <span class="material-input"></span>
                                    </div>
                    <!--fb/ig-->     <div class="form-group label-floating is-empty">
                                        <label class="control-label "><?php foreach($data_profil as $data){
                                            if($data->fb_ig==null){echo "Instagram";}
                                        }?></label>
                                        <input type="TEXT" class="form-control" name="fb_ig" value="<?php foreach($data_profil as $data){ echo $data->fb_ig;}?>"><?php foreach($data_profil as $data){
                                            if($data->fb_ig==null){}else{echo "(Instagram)";}
                                        }?>
                                        <span class="material-input"></span>
                                    </div>
                     <!--Pemurid-->     <div class="form-group label-floating is-empty">
                                        <label class="control-label "><?php foreach($data_profil as $data){
                                            if($data->pemurid==null){echo "pemurid";}
                                        }?></label>
                                        <input type="TEXT" class="form-control" name="pemurid" value="<?php foreach($data_profil as $data){ echo $data->pemurid;}?>"><?php foreach($data_profil as $data){
                                            if($data->pemurid==null){}else{echo "(pemurid)";}
                                        }?>
                                        <span class="material-input"></span>
                                    </div>
                     <!--m1-->     <div class="form-group label-floating is-empty">
                                        <label class="control-label "><?php foreach($data_profil as $data){
                                            if($data->m1==null){echo "m1";}
                                        }?></label>
                                        <input type="TEXT" class="form-control" name="m1" value="<?php foreach($data_profil as $data){ echo $data->m1;}?>"><?php foreach($data_profil as $data){
                                            if($data->m1==null){}else{echo "(m1)";}
                                        }?>
                                        <span class="material-input"></span>
                                    </div>
                    <!--pelayanan-->     <div class="form-group label-floating is-empty">
                                        <label class="control-label "><?php foreach($data_profil as $data){
                                            if($data->alokasi==null){echo "pelayanan";}
                                        }?></label>
                                        <input type="TEXT" class="form-control" name="alokasi" value="<?php foreach($data_profil as $data){ echo $data->alokasi;}?>"><?php foreach($data_profil as $data){
                                            if($data->alokasi==null){}else{echo "(alokasi)";}
                                        }?>
                                        <span class="material-input"></span>
                                    </div>
                    <!--foto-->     <div class="form-group label-floating is-empty">
                                        <input type="file"  name="foto" value="<?php foreach($data_profil as $data){ echo $data->foto;}?>">Klik untuk upload foto
                                    </div>

                                    <div class="card-footer justify-content-center card-profile">
                                                    <button type="submit"  name="submit" value="submit" class="btn btn-just-icon btn-whatsapp btn-round" data-background-color="green">Simpan
                                                        <i class="fa fa-edit"></i>
                                                    </button>
                                            </div>
                                            
                                <?php echo form_close(); ?>
                                            
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
						
                    </div>
                </div>
            </div>
