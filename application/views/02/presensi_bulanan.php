<div class="main-panel">
            <nav class="navbar navbar-transparent navbar-absolute">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#"> Presensi </a>
                    </div>
                    <div class="collapse navbar-collapse">
                        <ul class="nav navbar-nav navbar-right">
                            <li>
                                <a href="#pablo" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="material-icons">dashboard</i>
                                    <p class="hidden-lg hidden-md">Dashboard</p>
                                </a>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="material-icons">notifications</i>
                                    <span class="notification">5</span>
                                    <p class="hidden-lg hidden-md">Notifications</p>
                                </a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <a href="#">Mike John responded to your email</a>
                                    </li>
                                    <li>
                                        <a href="#">You have 5 new tasks</a>
                                    </li>
                                    <li>
                                        <a href="#">You're now friend with Andrew</a>
                                    </li>
                                    <li>
                                        <a href="#">Another Notification</a>
                                    </li>
                                    <li>
                                        <a href="#">Another One</a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="#pablo" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="material-icons">person</i>
                                    <p class="hidden-lg hidden-md">Profile</p>
                                </a>
                            </li>
                        </ul>
                        <form class="navbar-form navbar-right" role="search">
                            <div class="form-group  is-empty">
                                <input type="text" class="form-control" placeholder="Search">
                                <span class="material-input"></span>
                            </div>
                            <button type="submit" class="btn btn-white btn-round btn-just-icon">
                                <i class="material-icons">search</i>
                                <div class="ripple-container"></div>
                            </button>
                        </form>
                    </div>
                </div>
            </nav>
            <div class="content">
                <div class="container-fluid">
					
                    <div class="row">
                        
						<div class="col-md-12">
                            <div class="card card-plain">
                                <div class="card-header" data-background-color="blue">
                                    <h4 class="title">Data Presensi</h4>
                                    <p class="category"></p>
									<ul class="nav nav-tabs" data-tabs="tabs" data-background-color="blue">
                                      <li class="">
                                         <form action="<?php echo base_url('/index.php/dashboard02/report_presensi_bulanan');?>" method="post" >
											<input type="date" name="front" class="form-control" onchange="this.form.submit();" ></input><br>
                                            <input type="date" name="rear" class="form-control" onchange="this.form.submit();" ></input>
													
                                      </li>
                                      <li class="">
										 <button type="submit" class="btn btn-warning" name="submit" value="submit"><i class="material-icons">people</i>Pilih </button>
										</form>
                                    </ul>
                                </div>
                                <div class="card-content table-responsive">
                                    <table class="table table-hover">
                                        <thead>
                                            <th>No</th>
                                            <th>Name</th>
                                            <th>Gender</th>
											<th>Tgl Hadir</th>
                                            <th>Jam Hadir</th>
                                            <th>tlp/wa</th>
                                        </thead>
                                        <tbody>
										<?php
									$no=1;
									if($data_presensi==NULL){?>
									<div class="alert alert-error">
									<button type="button" class="close" data-dismiss="alert">x</button>
									<h4>Peringatan</h4>
									<p>Peringatan</p>
									</div>
									<?php }else{foreach($data_presensi as $dg){?>
                                            <tr>
                                            <td><?php echo $no++;?></td>
											<td><?php echo $dg->nama_jemaat;?></td>
											<td><?php echo $dg->jenis_kelamin;?></td>
											<td><?php echo $dg->tgl_presensi;?></td>
											<td><?php echo $dg->jam_presensi;?></td> 
											<td><a href="https://api.whatsapp.com/send?phone=62<?php echo ltrim(($dg->tlp_jemaat),'0');?>&text=Saya"><?php echo $dg->tlp_jemaat;?></a></td>  
                                            </tr>
											<?php	}	}?>
											<!--https://api.whatsapp.com/send?phone=6287893811922&text=Saya%20ingin
%20pesan%20Amoorea%20Aura%201box.%20Minta%20Rekeningnya-->

                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
						
                    </div>
                </div>
            </div>