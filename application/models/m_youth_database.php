<?php
ob_start();
class m_youth_database extends CI_Model{

	function __construct(){

		parent::__construct();

		$this->tbadmin="tb_admin";

		$this->tbjemaat="tb_jemaat";

		$this->tbdadakan="tb_dadakan";
		

	}

	function cek_admin($username="",$password=""){

		$query=$this->db->get_where($this->tbadmin,array('username'=>$username,'password'=>$password));

		$query=$query->result_array();

		return $query;

	}

	function ambil_admin($username){

		$query=$this->db->get_where($this->tbadmin,array('username'=>$username));

		$query=$query->result_array();

		if($query){

			return $query[0];

		}

	}

	function ambil_data_youth($status){

		$this->db->order_by("nama_jemaat","asc");

		$ambil_data_youth=$this->db->get_where($this->tbjemaat,array('status'=>$status));

		

		if($ambil_data_youth->num_rows()>0){

			foreach($ambil_data_youth->result() as $data){

				$hasil[]=$data;

				

			} return $hasil;

		}

	}

	function insert(){

		$id_kfc=$this->input->post('id_kfc');

		$nama_jemaat=$this->input->post('nama');

		$jenis_kelamin=$this->input->post('jenis_kelamin');

		$tgl_lahir=$this->input->post('tgl_lahir');

		$alamat_jemaat=$this->input->post('alamat');

		$tlp_jemaat=$this->input->post('tlp');

		$bbm_wa=$this->input->post('bbm_wa');

		$tgl_masuk=$this->input->post('tgl_masuk');

		$fb_ig=$this->input->post('fb_ig');

		$komsel=$this->input->post('komsel');

		$status=1;

		$tgl_input=date("Y-m-d");

		

		$data=array(

					'id_kfc'=>$id_kfc,

					'nama_jemaat'=>$nama_jemaat,

					'jenis_kelamin'=>$jenis_kelamin,

					'tgl_lahir'=>$tgl_lahir,

					'alamat_jemaat'=>$alamat_jemaat,

					'tlp_jemaat'=>$tlp_jemaat,

					'bbm_wa'=>$bbm_wa,

					'tgl_masuk'=>$tgl_masuk,

					'fb_ig'=>$fb_ig,

					'komsel'=>$komsel,

					'status'=>$status,

					'tgl_input'=>$tgl_input,
					'last_presensi'=>$tgl_input

					);

		

		$this->db->insert('tb_jemaat',$data);

	}

	function id_kfc() {

		$value = $this->db->query("SELECT max(id_kfc) as idkfc FROM tb_jemaat ORDER BY id_kfc DESC");

			$row = $value->row_array();

			if ($value != "") { // jika sudah ada, langsung ambil dan proses...

				$max_id=$row['idkfc'];

				$sLastKode =(int)substr($max_id, 3, 4); // ambil 3 digit terakhir

				$sLastKode = $sLastKode + 1; // konversi ke integer, lalu tambahkan satu

				$id_kfc = "KFC" . sprintf('%04s', $sLastKode); // format hasilnya dan tambahkan prefix

				if (strlen($id_kfc) > 7) {

					$id_kfc = "KFC9999";

				}

			} else { // jika belum ada, gunakan kode yang pertama

				$id_kfc = "KFC0001";

			}

			return $id_kfc;

		 

	}

	function getById($id_kfc){

		return $this->db->get_where('tb_jemaat',array('id_kfc'=>$id_kfc))->row();

	}

	function edit($upload,$id_kfc){

		$id_kfc=$this->input->post('id_kfc');

		$nama_jemaat= $this->input->post('nama');

		$tgl_lahir= $this->input->post('tgl_lahir');

		$alamat_jemaat= $this->input->post('alamat_jemaat');

		$tlp_jemaat= $this->input->post('tlp');

		$bbm_wa= $this->input->post('bbm_wa');

		$fb_ig= $this->input->post('fb_ig');
                
        $komsel= $this->input->post('komsel');

		$jenis_kelamin= $this->input->post('jenis_kelamin');

		$foto= $upload['file']['file_name'];

		$data=array(

					'nama_jemaat'=>$nama_jemaat,

					'alamat_jemaat'=>$alamat_jemaat,

					'tlp_jemaat'=>$tlp_jemaat,

					'bbm_wa'=>$bbm_wa,

					'fb_ig'=>$fb_ig,
                                        
                                        'komsel'=>$komsel,

					'id_kfc'=>$id_kfc,

					'tgl_lahir'=>$tgl_lahir,

					'jenis_kelamin'=>$jenis_kelamin,
					'foto'=>$foto

					);

		$this->db->where('id_kfc',$id_kfc);

		$this->db->update($this->tbjemaat,$data);

	}

	function hapus($id_kfc){ 

		$this->db->where('id_kfc',$id_kfc);

		$this->db->delete('tb_jemaat');

		return;

	}

	function simpan_presensi(){

		$id_kfc=$this->input->post('id_kfc');

		$nama_jemaat=$this->input->post('nama_jemaat');

		$tgl_lahir=$this->input->post('tgl_lahir');

		$tgl_presensi=$this->input->post('tgl_presensi');

		$presensi=$this->input->post('presensi');
		$peran=$this->input->post('peran');

		$tgl_input=date("Y-m-d");

		$status=$this->input->post('status');

		$absen=$this->input->post('absen');

		$jumlahdata=count($id_kfc);

		for($x=0;$x<$jumlahdata;$x++){

			if($presensi[$x]!="1"){

				$absen[$x]=$absen[$x]+1;

				if($absen[$x]>10){

					$status[$x]="0";

				}

			}else{

				$absen[$x]=$absen[$x]-1;

				if($absen[$x]<="0"){

					$absen[$x]="0";

				}

				}

				if($presensi[$x]=="1"){

			$this->db->query("insert into tb_presensi(id_kfc, presensi, tgl_presensi, nama_jemaat, tgl_input, tgl_lahir,peran) value('$id_kfc[$x]','$presensi[$x]','$tgl_presensi','$nama_jemaat[$x]','$tgl_input','$tgl_lahir[$x]','$peran[$x]')");
		}

			$this->db->query("update tb_jemaat set last_presensi='$tgl_presensi' where id_kfc='$id_kfc[$x]'");
			//$this->db->query("update tb_jemaat set absen='$absen[$x]',status='$status[$x]' where id_kfc='$id_kfc[$x]'");<----metode

		}

	}

	function ambil_data_ultah($bln_lahir,$li,$of){

		$this->db->order_by("day(tgl_lahir)","asc");

		$ambil_data_ultah=$this->db->get_where($this->tbjemaat,array('month(tgl_lahir)'=>$bln_lahir,'status'=>'1'),$li,$of);

		if($ambil_data_ultah->num_rows()>0){

			foreach($ambil_data_ultah->result() as $data){

				$hasil[]=$data;

				

			} return $hasil;

		}

	}

	function ambil_data_keluarga_baru($bln_masuk,$thn_masuk){

		
		$ambil_data_keluarga_baru=$this->db->get_where($this->tbjemaat,array('month(tgl_masuk)'=>$bln_masuk,'year(tgl_masuk)'=>$thn_masuk));

		if($ambil_data_keluarga_baru->num_rows()>0){

			foreach($ambil_data_keluarga_baru->result() as $data){

				$hasil[]=$data;

			} return $hasil;

		}

	}
	function info_keluarga_baru($tgl_awal,$tgl_akhir){
		//$ambil_data_keluarga_baru=$this->db->query("select * from tb_jemaat where tgl_masuk between $tgl_awal and $tgl_akhir order by tgl_masuk asc");
		$per_awal=mktime(0,0,0,date("n")-1,-2,date("Y")); //n=bulan, Y=tahun, j=day
		$awal=date("Y-m-d",$per_awal);

		$this->db->where('tgl_masuk >=',$awal);
		$this->db->where('tgl_masuk <=',$tgl_akhir);
		$ambil_data_keluarga_baru=$this->db->get($this->tbjemaat);
			if($ambil_data_keluarga_baru->num_rows()>0){
			foreach($ambil_data_keluarga_baru->result() as $data){
				$hasil[]=$data;
			} return $hasil;

		}

	}
	function progres_keluarga_baru($tgl_awal,$tgl_akhir){
		//$ambil_data_keluarga_baru=$this->db->query("select * from tb_jemaat where tgl_masuk between $tgl_awal and $tgl_akhir order by tgl_masuk asc");
		$per_awal=mktime(0,0,0,date("n")-1,-2,date("Y")); //n=bulan, Y=tahun, j=day
		$awal=date("Y-m-d",$per_awal);

		//$this->db->where('tgl_masuk >=',$awal);
		//$this->db->where('tgl_masuk <=',$tgl_akhir);
		//$ambil_data_keluarga_baru=$this->db->get($this->tbjemaat);
			//if($ambil_data_keluarga_baru->num_rows()>0){
			//foreach($ambil_data_keluarga_baru->result() as $data){
			//	$hasil[]=$data;
			//} return $hasil;

		//}

		$this->db->select("*")    
                     ->from('tb_fu')
                     ->join('tb_jemaat', 'tb_jemaat.id_kfc=tb_fu.id_kfc','inner')
                     ->where('tb_fu.tgl_masuk_fu >=',$awal)
                     ->where('tb_fu.tgl_masuk_fu <=',$tgl_akhir)
                     ->order_by('tb_fu.tgl_masuk_fu','desc');
            $query =$this->db->get();
            return $query->result();   

	}
	function fu($id_kfc){
		$this->db->select("*")    
                     ->from('tb_fu')
                     ->join('tb_jemaat', 'tb_jemaat.id_kfc=tb_fu.id_kfc','inner')
                     ->where('tb_fu.id_kfc',$id_kfc);
            $query =$this->db->get();
            return $query->result();
	}

	function aktivasi($id_kfc){

		$status="1";

		$absen="0";
		$last_presensi=date("Y-m-d");

		$data=array('status'=>$status,

					'id_kfc'=>$id_kfc,
					'last_presensi'=>$last_presensi,

					'absen'=>$absen,);

		$this->db->where('id_kfc',$id_kfc);

		$this->db->update($this->tbjemaat,$data);
	}	
	/////report new member
	function ambil_report_new_member(){
						$this->db->select('tb_jemaat.id_kfc,tb_jemaat.nama_jemaat,tb_presensi.presensi,tb_presensi.tgl_presensi')->join('tb_jemaat','tb_jemaat.id_kfc=tb_presensi.id_kfc');
						return $this->db->get('tb_jemaat')->result();

						$ambil_report_new_member=$this->db->get_where($this->tbjemaat,array('month(tgl_masuk)'=>$bln_masuk,'year(tgl_masuk)'=>$thn_masuk));

						if($ambil_report_new_member->num_rows()>0){

						foreach($ambil_report_new_member->result() as $data){

						$hasil[]=$data;

				

						} return $hasil;

						}
	}
	//////bagian untuk database komsel		
	function ambil_data_komsel($komsel){		
		$this->db->order_by("nama_jemaat","asc");		
		$ambil_data_ultah=$this->db->get_where($this->tbjemaat,array('komsel'=>$komsel));		
		if($ambil_data_ultah->num_rows()>0){			
			foreach($ambil_data_ultah->result() as $data){				
				$hasil[]=$data;							
			} return $hasil;		
		}	
	}

	function simpan_direct_presensi(){

		$id_kfc=$this->input->post('id_kfc');
		if($this->input->post('alokasi')==null){$peran="";}else{$peran=$this->input->post('alokasi');}
		$tgl_presensi=$this->input->post('tgl_presensi');

		$tgl_input=date("Y-m-d");
		date_default_timezone_set('Asia/Jakarta');

		date();
		$jam_presensi=date("h:i:sa");
		$pelayan=$this->input->post('pelayan');

		if($id_kfc==null){
			print "<script type=\"text/javascript\">alert('Some text');</script>";
			echo"<script>alert('belum')</script>";
			
			redirect("dashboard02/direct_presensi");
		}else{
			$dicek=$this->db->query("select * from direct_presensi where id_kfc='$id_kfc' and tgl_presensi='$tgl_presensi'");
			if($dicek->num_rows()>0){
				redirect("dashboard02/direct_presensi");
			}else{
					$this->db->query("insert into direct_presensi (id_kfc, tgl_presensi, tgl_input,jam_presensi,peran) value ('$id_kfc','$tgl_presensi','$tgl_input','$jam_presensi','$peran')");
					$this->db->query("update tb_jemaat set last_presensi='$tgl_presensi',alokasi='' where id_kfc='$id_kfc'");
					
					$this->db->query("update tb_jemaat set status='1' where id_kfc='$id_kfc'");
					$this->session->set_flashdata('msg', 
              		  '<div class="alert alert-success">
                    	<h4>Terima Kasih</h4>
						<p>Selamat datang</p>
						
               			 </div>');
				}
		
		}
		

	}
	function simpan_direct_presensi_usher(){

		$id_kfc=$this->input->post('id_kfc');
		$tgl_presensi=$this->input->post('tgl_presensi');

		$tgl_input=date("Y-m-d");
		date_default_timezone_set('Asia/Jakarta');
		date();
		$jam_presensi=date("h:i:sa");
		$rah=$this->input->post('rah');
		$manado=$this->input->post('manado');
		$dojar=$this->input->post('dojar');

		if($id_kfc==null){
			print "<script type=\"text/javascript\">alert('Some text');</script>";
			echo"<script>alert('belum')</script>";
			
			redirect("dashboard/direct_presensi");
		}else{
			$dicek=$this->db->query("select * from direct_presensi where id_kfc='$id_kfc' and tgl_presensi='$tgl_presensi'");
			if($dicek->num_rows()>0){
				redirect("dashboard/direct_presensi");
			}else{
					$this->db->query("insert into direct_presensi (id_kfc, tgl_presensi, tgl_input,jam_presensi,rah,manado,dojar) value ('$id_kfc','$tgl_presensi','$tgl_input','$jam_presensi','$rah','$manado','$dojar')");
					$this->db->query("update tb_jemaat set last_presensi='$tgl_presensi',alokasi='' where id_kfc='$id_kfc'");
					
					$this->db->query("update tb_jemaat set status='1' where id_kfc='$id_kfc'");
				}
		
		}
		

	}
	function show_presensi($now){
		
		 $this->db->select("*")    
                     ->from('direct_presensi')
                     ->join('tb_jemaat', 'tb_jemaat.id_kfc=direct_presensi.id_kfc','inner')
                     ->where('direct_presensi.tgl_presensi',$now)
                     ->order_by('direct_presensi.jam_presensi','desc');
            $query =$this->db->get();
            return $query->result();   
	}
	function show_presensi_bulanan($front,$rear){
		
		 $this->db->select("*")    
		 			->distinct("id_kfc")
                     ->from('direct_presensi')
                     ->join('tb_jemaat', 'tb_jemaat.id_kfc=direct_presensi.id_kfc','inner')
                     ->where('direct_presensi.tgl_presensi >=',$front)
                     ->where('direct_presensi.tgl_presensi <=',$rear)
                     ->order_by('direct_presensi.tgl_presensi','desc')
                     ->group_by('direct_presensi.id_kfc');

            $query =$this->db->get();
            return $query->result();   
	}
	function simpan_alokasi(){

		$id_kfc=$this->input->post('id_kfc');
		$alokasi=$this->input->post('alokasi');
		if($id_kfc==null){
			print "<script type=\"text/javascript\">alert('Some text');</script>";
			echo"<script>alert('belum')</script>";
			
			redirect("dashboard/alokasi");
			}else{
					$this->db->query("update tb_jemaat set alokasi='$alokasi' where id_kfc='$id_kfc'");
			}

	}
		function show_alokasi(){
		
		 $query=$this->db->query("select * from tb_jemaat where alokasi !=''");
            if($query->num_rows()>0){

			foreach($query->result() as $data){

				$hasil[]=$data;

				

			} return $hasil;

		}  
	}
	function insert_mbuh(){
	if($this->input->post('nama')==null){
	$this->session->set_flashdata('msg', 
                '<div class="alert alert-danger">
                    <h4>Oppss</h4>
                    <p>Silahkan ulangi lagi. Tidak ada kata dinput.</p>
                </div>');
			redirect('dashboard02/mbuh','refresh');
	}else{
		//$id_kfc=$this->input->post('id_kfc'); <-sudah tidak dipakai, id diinjek saat proses save, bukan saat open form
		$id_kfc=$this->id_kfc();
		
		$nama=$this->input->post('nama');

		$kelamin=$this->input->post('kelamin');

		$tgl=$this->input->post('tgl');
		
		$bln=$this->input->post('bln');
		
		$ig=$this->input->post('fb_ig');
		
		$wa=$this->input->post('bbm_wa');

		$thn=$this->input->post('thn');
		
		$alamat=$this->input->post('alamat');

		$tlp=$this->input->post('tlp');

		$tgl_input=date("Y-m-d");
		
		$tgl_lahir=$thn."-".$bln."-".$tgl;
		
		date_default_timezone_set('Asia/Jakarta');
		
		$jam_presensi=date("h:i:sa");
		
		$status=1;
		$data=array(

					'id_kfc'=>$id_kfc,
					
					'bbm_wa'=>$wa,
					
					'fb_ig'=>$ig,
					
					'nama_jemaat'=>$nama,

					'jenis_kelamin'=>$kelamin,

					'tgl_lahir'=>$tgl_lahir,

					'alamat_jemaat'=>$alamat,

					'tlp_jemaat'=>$tlp,
					'status'=>$status,
					
					'tgl_masuk'=>$tgl_input,

					'tgl_input'=>$tgl_input	);
		$presensi=array(

					'id_kfc'=>$id_kfc,
										
					'tgl_presensi'=>$tgl_input,
					
					'jam_presensi'=>$jam_presensi,

					'tgl_input'=>$tgl_input	);
		$fkb= array('id_kfc'=>$id_kfc,
					'tgl_masuk_fu'=>$tgl_input, );

				$this->db->insert('tb_jemaat',$data);
				$this->db->insert('direct_presensi',$presensi);
				$this->db->insert('tb_fu',$fkb);
}

	}
function cek_mbuh(){
$this->db->order_by("nama","asc");
$tgl_input=date("Y-m-d");
		$ambil_data_youth=$this->db->get_where($this->tbdadakan,array('tgl_input'=>$tgl_input));
		if($ambil_data_youth->num_rows()>0){
			foreach($ambil_data_youth->result() as $data){
				$hasil[]=$data;
			} return $hasil;

		}
}
  function ranking(){
		$tgl_ini=date("Y-m-d");
		$per_akhir=mktime(0,0,0,date("n"),0,date("Y"));
		$akhir=date("Y-m-d",$per_akhir);
		$per_awal=mktime(0,0,0,date("n")-3,+1,date("Y"));
		$awal=date("Y-m-d",$per_awal);
		
      $query=$this->db->query("select direct_presensi.id_kfc, tb_jemaat.nama_jemaat, COUNT( DISTINCT  direct_presensi.tgl_presensi ) AS jml from direct_presensi, tb_jemaat where direct_presensi.tgl_presensi between '$awal' and '$akhir' and direct_presensi.id_kfc=tb_jemaat.id_kfc group by direct_presensi.id_kfc order by jml desc limit 0,10");
	  if($query->num_rows()>0){

			foreach($query->result() as $data){

				$hasil[]=$data;

				

			} return $hasil;

		}  
}
function jemaat_absen($la){
	$tgl_ini=date("Y-m-d");
	$threemonth=mktime(0,0,0,date("n")-3,date("j"),date("Y"));
	$tgl_lalu=date("Y-m-d",$threemonth);
	
	$query=$this->db->query("select direct_presensi.id_kfc, tb_jemaat.nama_jemaat,tb_jemaat.jenis_kelamin,tb_jemaat.tlp_jemaat,tb_jemaat.tgl_masuk, COUNT( DISTINCT  direct_presensi.tgl_presensi ) AS jml,('12'-COUNT( DISTINCT  direct_presensi.tgl_presensi )) as absen from direct_presensi, tb_jemaat where direct_presensi.tgl_presensi between '$tgl_lalu' and '$tgl_ini' and tb_jemaat.tgl_masuk between '2017-11-01' and '2018-02-03' and direct_presensi.id_kfc=tb_jemaat.id_kfc group by direct_presensi.id_kfc order by tb_jemaat.tgl_masuk desc limit 0,$la");
	  if($query->num_rows()>0){

			foreach($query->result() as $data){

				$hasil[]=$data;

				

			} return $hasil;

		}  
}
function odometer_j(){
	$query=$this->db->query("select*from tb_jemaat");
	$total=$query->num_rows();
	return $total;
}
function odometer_j_a(){
	$query=$this->db->query("select*from tb_jemaat where status='1'");
	$total=$query->num_rows();
	return $total;
}
function odometer_j_b(){
	$tgl_ini=date("Y-m-d");
	$query=$this->db->query("select*from tb_dadakan where tgl_input='$tgl_ini' ");
	$total=$query->num_rows();
	return $total;
}
function odometer_p(){
	$date=date("Y-m-d");
	$query=$this->db->query("select*from direct_presensi where tgl_presensi='$date'");
	$total=$query->num_rows();
	return $total;
}
function kenaikan_presensi(){
	$query=$this->db->query("SELECT DISTINCT(tgl_presensi) as tot,(select count(id_kfc) from direct_presensi where tgl_presensi= tot) as jml from direct_presensi where tgl_presensi between '2017-11-01' and '2018-01-27'");
	if($query->num_rows()>0){

			foreach($query->result() as $data){

				$hasil[]=$data;

				

			} return $hasil;

		}  
}
function pro_awal(){
	$q=mktime(0,0,0,date("n"),date("j")-7,date("Y"));
	$d=date("Y-m-d",$q);
	
	$query=$this->db->query("SELECT id_kfc from direct_presensi where tgl_presensi='$d' ");
	$total=$query->num_rows();
	return $total;
}
function pro_sekarang(){
	$d=date("Y-m-d");
	
	$query=$this->db->query("SELECT id_kfc from direct_presensi where tgl_presensi='$d' ");
	$total=$query->num_rows();
	return $total;
}
//upload gambar
	public function upload()
{
    $config['upload_path']          = './foto/';
    $config['allowed_types']        = 'gif|jpg|png|jpeg';
    $config['file_name']            = $this->input->post('id_kfc');
    $config['overwrite']			= true;
    $config['max_size']             = 10024; // 1MB
    $config['remove_space']			=TRUE;
    // $config['max_width']            = 1024;
    // $config['max_height']           = 768;
    //////
    $this->load->library('upload',$config);
		if($this->upload->do_upload('foto')){
			$return=array('result'=>'success','file'=>$this->upload->data(),'error'=>'');
			return $return;
		}else{
			$return=array('result'=>'failed','file'=>'','error'=>$this->upload->display_errors());
			return $return;
		}
} 
public function profil($id){
	return $this->db->get_where('tb_jemaat', array('id_kfc'=>"$id"))->result();
}
public function save($upload){
		$data=array(
			'id_kfc'=>$this->input->post('id_kfc'),
			'foto'=>$upload['file']['file_name'],
			'nama_jemaat'=>$this->input->post('nama_jemaat'),
			'alamat_jemaat'=>$this->input->post('alamat_jemaat'),
			'tlp_jemaat'=>$this->input->post('tlp_jemaat'),
			'bbm_wa'=>$this->input->post('bbm_wa'),
			'jenis_kelamin'=>$this->input->post('jenis_kelamin'),
			'komsel'=>$this->input->post('komsel'),
			'pemurid'=>$this->input->post('pemurid'),
			'm1'=>$this->input->post('m1'),
			'alokasi'=>$this->input->post('alokasi'),
			'fb_ig'=>$this->input->post('fb_ig'));
		$this->db->where('id_kfc',$this->input->post('id_kfc'));

		$this->db->update($this->tbjemaat,$data);
		//$this->db->insert('gambar',$data);
	}
public function save1($upload){
		$data=array(
			'id_kfc'=>$this->input->post('id_kfc'),
			'jenis_kelamin'=>$this->input->post('jenis_kelamin'),
			'nama_jemaat'=>$this->input->post('nama_jemaat'),
			'alamat_jemaat'=>$this->input->post('alamat_jemaat'),
			'tlp_jemaat'=>$this->input->post('tlp_jemaat'),
			'bbm_wa'=>$this->input->post('bbm_wa'),
			'komsel'=>$this->input->post('komsel'),
			'pemurid'=>$this->input->post('pemurid'),
			'm1'=>$this->input->post('m1'),
			'alokasi'=>$this->input->post('alokasi'),
			'fb_ig'=>$this->input->post('fb_ig'));
		$this->db->where('id_kfc',$this->input->post('id_kfc'));

		$this->db->update($this->tbjemaat,$data);
		//$this->db->insert('gambar',$data);
	}
public function simpan_fu(){
		
		$id_kfc=$this->input->post('id_kfc');
		$fu=$this->input->post('fu');
		$i=$this->input->post('i');
		$ii=$this->input->post('ii');
		$iii=$this->input->post('iii');
		$goal=$this->input->post('goal');
		if($id_kfc==null){
			print "<script type=\"text/javascript\">alert('Some text');</script>";
			echo"<script>alert('belum')</script>";
			
			redirect("dashboard/alokasi");
			}else{
					$this->db->query("update tb_fu set fu='$fu',i='$i',ii='$ii',iii='$iii',goal='$goal' where id_kfc='$id_kfc'");
			}
	}
}

?>		